export class MimeType {
    extension: string;
    contentType: string;
}

export const MimeTypes: Array<MimeType> = [
    {
        extension: ".html",
        contentType: "text/html"
    },
    {
        extension: ".js",
        contentType: "text/javascript"
    },
    {
        extension: ".css",
        contentType: "text/css"
    },
    {
        extension: ".png",
        contentType: "image/png"
    },
    {
        extension: ".jpg",
        contentType: "image/jpg"
    },
    {
        extension: ".woff",
        contentType: "text/font-woff"
    },
    {
        extension: ".ttf",
        contentType: "application/font-ttf"
    },
    {
        extension: ".eot",
        contentType: "application/vnd.ms-fontobject"
    },
    {
        extension: ".otf",
        contentType: "application/font-otf"
    },
    {
        extension: ".svg",
        contentType: "application/image/svg+xml"
    },
    {
        extension: ".ico",
        contentType: "image/x-icon"
    }
];