import {MimeType} from "./mime-types";

export class ParsedRequest {
    isFile: boolean;
    filePath: string;
    mimeType: MimeType;
    routePieces: string[];
    queryString: string;
    fullUrl: string;
    verb: string;
    headers: any[];
    body: any;
}