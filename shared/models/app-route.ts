export class AppRoute {
    path: string[];
    optionalParams: string[];
    controller: Function;
}