import {MimeType, MimeTypes} from "../shared/models/mime-types";
import fs from "fs";
import path from "path";
import {ParsedRequest} from "../shared/models/parsed-request";


export class RequestParser {

    parse(request) {

        const parsedRequest = new ParsedRequest();

        let filePath = request.url.indexOf("/dist/client") === 0 ?
            "." + request.url :
            "./dist/client" + request.url;

        filePath = filePath.indexOf("?") > -1 ? filePath.split("?")[0] : filePath;

        if(filePath === "./dist/client/"){
            filePath = "./dist/client/index.html";
        }

        const extension = String(path.extname(filePath)).toLowerCase();
        const mime = MimeTypes.find((x:MimeType) => x.extension === extension);
        const slashSplit = request.url.split("/");

        parsedRequest.isFile = mime && mime.contentType !== null;
        parsedRequest.filePath = filePath;
        parsedRequest.routePieces = [];
        if(!parsedRequest.isFile) {
            parsedRequest.routePieces = slashSplit.slice(1);
        }
        parsedRequest.fullUrl = request.url;
        parsedRequest.mimeType = mime;
        parsedRequest.headers = request.headers;
        parsedRequest.verb = request.method;
        parsedRequest.queryString = request.url.indexOf("?") > -1 ? request.url.split("?")[1] : null;

        return parsedRequest

    }

}