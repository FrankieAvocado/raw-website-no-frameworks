import {AppRoute} from "../shared/models/app-route";
import {ParsedRequest} from "../shared/models/parsed-request";

export class Router {

    private _routes : AppRoute[];

    addRoute(path: string, controller: Function) {
        this._routes = this._routes || new Array<AppRoute>();

        const paramsSplit = path.split("[");
        let optionalParams = [];
        if(paramsSplit.length > 1) {
            optionalParams = paramsSplit.slice(1).map(
                x => x.replace("[", "")
                    .replace("]", "")
            );
            console.log("optional params: ", optionalParams);
        }

        this._routes.push({
            path: paramsSplit[0].split("/").slice(1).filter(x => x !== ""),
            optionalParams,
            controller
        } as AppRoute);
    }

    linkToController(parsedRequest: ParsedRequest, response) {

        const numericallyValidRoutes = this._routes.filter(
            route =>
                route.optionalParams.length + route.path.length
                    >= parsedRequest.routePieces.length
        );

        let foundRoute = null;

        for(let route of numericallyValidRoutes) {
            let isMatch = true;
            for(let i = 0; i < route.path.length; i++){
               if(route.path[i] !== parsedRequest.routePieces[i]){
                   isMatch = false;
                   break;
               }
            }

            if(isMatch === true) {
                foundRoute = route;
                foundRoute.params = [];
                for(let i = 0; i < route.optionalParams.length; i++){
                    if(parsedRequest.routePieces.length > i + route.path.length){
                        foundRoute.params.push(parsedRequest.routePieces[i + route.path.length]);
                    }
                }
                break;
            }
        }

        if(foundRoute) {
            foundRoute.controller(response, foundRoute.params)
        } else {
            response.writeHead(404);
            response.end("Unable to find requested route!");
            response.end();
        }
    }

}