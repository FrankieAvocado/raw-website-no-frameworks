import {TestClass} from "../shared/test";
import http from "http";
import {RequestParser} from "./request-parser";
import {StaticFileServer} from "./static-file-server";
import {Router} from "./router";

const newTest = new TestClass();
newTest.test = "Yo Globe!";

console.log(newTest.test);

const port = process.env.PORT || 3000;
const requestParser = new RequestParser();
const staticFileServer = new StaticFileServer();
const router = new Router();

router.addRoute("/hello/world/[:id]", (response, id) => {
    console.log("yay!", id);
    response.writeHead(200);
    response.end();
});

const server = http.createServer((request, response) => {
   const parsedRequest = requestParser.parse(request);

   if(parsedRequest.isFile){
       staticFileServer.serve(parsedRequest, response);
   } else {
       router.linkToController(parsedRequest, response);
       response.writeHead(200);
       response.end();
   }

});

server.listen(port);

console.log(`The audience is listening...on port ${port}`);
