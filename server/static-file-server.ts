import {ParsedRequest} from "../shared/models/parsed-request";
import fs from "fs";

export class StaticFileServer {
    serve(parsedRequest: ParsedRequest, response) {
        if(parsedRequest.isFile){
            fs.readFile(parsedRequest.filePath, (error, content) => {
                if (error) {
                    console.log(error);
                    response.writeHead(500);
                    response.end("Everything exploded, my bad");
                    response.end();
                } else {
                    response.writeHead(200, {"Content-Type": parsedRequest.mimeType.contentType});
                    response.end(content, "utf-8");
                }
            })
        } else {
            throw "Tried to statically serve something that wasn't a file!";
        }
    }
}